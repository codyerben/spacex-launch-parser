
#include "../../spacex-launch-parser/inc/myclass.hpp"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;

TEST(SpaceXLaunchParserTests, FileSystem)
{
    MyClass a("abc");
    EXPECT_EQ("abc", a.GetId());
}
