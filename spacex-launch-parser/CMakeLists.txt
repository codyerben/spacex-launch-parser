cmake_minimum_required(VERSION 3.0)

set(This spacex-launch-parser)
project(${This} C CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(GCC_COVERAGE_COMPILE_FLAGS "-g -O0 -coverage -fprofile-arcs -ftest-coverage")
set(GCC_COVERAGE_LINK_FLAGS "-coverage -lgcov")

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY lib)

enable_testing()
include_directories(
    inc
)

# Some recommendations on NOT using file globbing to grab all cpp files.  So far, it seems
# ok, so rolling with it until we get more experience.

file(GLOB SPACEX_LAUNCH_PARSER_LIB_SRC
    "src/*.cpp"
)

file(GLOB SPACEX_LAUNCH_PARSER_APP_SRC
    "app/*.cpp"
)

add_library(${This} ${SPACEX_LAUNCH_PARSER_LIB_SRC})

add_executable(spacex-launch-parser-app ${SPACEX_LAUNCH_PARSER_APP_SRC})
