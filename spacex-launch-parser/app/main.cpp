////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Entrypoint for the SpaceX Launch Parser application.

*/

////////////////////////////////////////////////////////////////////////////////
#include <iostream>

int main() {
    const char* APPLICATION_GREETING = "Welcome to the SpaceX Launch Parser application.";

    std::cout << APPLICATION_GREETING << std::endl;
    return 0;
}
