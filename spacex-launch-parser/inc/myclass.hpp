#ifndef MYCLASS_H
#define MYCLASS_H

#include <iostream>
#include <list>
#include <string>

class MyClass
{
    public:

        MyClass(std::string id);

        std::string GetId();

        int getIdLen();

    private:

        std::string id_;
};

#endif 
